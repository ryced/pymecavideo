pymecavideo (6.2c-1) UNRELEASED; urgency=medium

  * upgraded to the new upstream version

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 08 Mar 2015 18:59:01 +0100

pymecavideo (6.2a-1) unstable; urgency=medium

  * upgraded to the new upstream version
  * added the dependency python-pyqtgraph

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 08 Mar 2015 11:51:44 +0100

pymecavideo (6.1a-1) unstable; urgency=medium

  * upgraded to the new upstream version
  * modified the way used to extract the version number, to avoid doubles
  * upgraded Standards-Version to 3.9.7
  * added a dependency on python-odf
  * fixed errors in oooexport.py

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 07 Mar 2015 17:49:29 +0100

pymecavideo (6.1-1) unstable; urgency=low

  * changed my DEBEMAIL
  * upgraded to the newest upstream release
  * simplified the patches managed by quilt

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 07 Feb 2013 13:18:24 +0100

pymecavideo (6.0-6) unstable; urgency=low

  * fixed a bad invocation of Zoom_Croix(), thanks to
    thomas.lavarenne@free.fr's bug report

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 12 Oct 2012 10:57:13 +0200

pymecavideo (6.0-5) unstable; urgency=low

  * updated to the latest version in branches/6.0 from the SVN

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 29 Jun 2012 22:12:07 +0200

pymecavideo (6.0-4) unstable; urgency=low

  * used a better approach to communicate with libreoffice
  * added the possibility to give a video filename as an argument on the
    command line
  * updated the manpages
  * 

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 24 Jun 2012 14:27:08 +0200

pymecavideo (6.0-3) unstable; urgency=low

  * upgraded to the latest SVN revision (in branch/6.0)
  * fixed an issue with {open|libre}office calc export.

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 23 Jun 2012 08:07:21 +0200

pymecavideo (6.0-2) unstable; urgency=low

  * taken in account the new command rsvg-convert. Closes: #666481

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 13 May 2012 21:41:07 +0000

pymecavideo (6.0-1) unstable; urgency=low

  * upgraded to the newest upstream version
  * upgraded Standards-Version to 3.9.3

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 12 May 2012 10:41:55 +0000

pymecavideo (5.5-5) unstable; urgency=low

  * added --with-python2 in debian/rules. Closes: #640580

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 06 Sep 2011 18:43:34 +0200

pymecavideo (5.5-4) unstable; urgency=low

  * upgraded Standards-Version to 3.9.2
  * removed an article in the synopsis
  * enforced the creation of the directory for personal data. Closes: #630140

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 11 Jun 2011 16:24:13 +0200

pymecavideo (5.5-3) unstable; urgency=low

  * changed the build-dependency rsvg -> librsvg2-bin. Closes: #629732

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 08 Jun 2011 19:08:22 +0200

pymecavideo (5.5-2) unstable; urgency=low

  * switched to dh_python2. Closes: #616972

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 07 Mar 2011 18:54:15 +0100

pymecavideo (5.5-1) unstable; urgency=low

  * modified the user interface (added a window icon, changed the place of
    two subwidgets).
  * changed the icon for the desktop file.

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 06 Mar 2011 04:04:20 +0100

pymecavideo (5.4-3) unstable; urgency=low

  * modifed the example video of the solar system

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 17 Feb 2011 19:18:05 +0100

pymecavideo (5.4-2) unstable; urgency=low

  * reinforced the test to know whether openCV has guessed the movie's
    features.
  * fixed a bug in cadreur.py which caused cropping errors
  * modified the lineEdit to display the scale : it allows now very 
    small scales (useful for the following example)
  * added an example video: a simulation of the solar system over one year,
    and a pymecavideo file to open it directly.

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 16 Feb 2011 16:02:13 +0100

pymecavideo (5.4-1) unstable; urgency=low

  * fixed the messages displaied during clics on the video images
  * fixed the recovery from a previously saved file
  * fixed the undo/redo feature near the first record 
  * improved the user interface
  * added a feature to recode badly managed video files

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 05 Feb 2011 17:43:08 +0100

pymecavideo (5.3-9) unstable; urgency=low

  * removed the dependency on the package ffmpeg coming from 
    debian-multimedia.org, Closes: #610708
  * removed any invokation of ffmpeg, by using openCV commands

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 02 Feb 2011 21:43:50 +0100

pymecavideo (5.3-8) unstable; urgency=low

  * added TAB chars for the copies into the clipboard
  * added a dependency on a non-buggy version of ffmpeg
  * modified detect.py to avoid the creation od spurious image files

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 14 Jan 2011 17:52:49 +0100

pymecavideo (5.3-7) unstable; urgency=low

  * modified the way pymecavideo creates directories under $(HOME)/.local
    now it creates them with the shell command "mkdir -p".

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 19 Dec 2010 17:45:08 +0100

pymecavideo (5.3-6) unstable; urgency=low

  * added a dependency on python-matplotlib. Closes: #606874

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 12 Dec 2010 20:17:57 +0100

pymecavideo (5.3-5) unstable; urgency=low

  * modified the importation of the module Error to disable its use 
    with GNU/Linux. Closes: #606380

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 12 Dec 2010 12:59:44 +0100

pymecavideo (5.3-4) unstable; urgency=low

  * changed the start time of the QApplication object to allow early 
    debugging.

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 18 Nov 2010 21:25:20 +0100

pymecavideo (5.3-3) unstable; urgency=low

  * taken in acount the possible use of rsvn which is way faster than
    inkscape to convert svg files.

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 13 Nov 2010 22:03:41 +0100

pymecavideo (5.3-2) unstable; urgency=low

  * added a shared-mimeinfo file
  * modified pymecavideo.desktop to take the new mime type in account

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 13 Nov 2010 18:33:32 +0100

pymecavideo (5.3-1) unstable; urgency=low

  * upgraded to the new upstream version

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 13 Nov 2010 11:56:45 +0100

pymecavideo (5.2-7) unstable; urgency=low

  * sanitized quilt patches (removing patches added by debuild)
  * enforced again a dependency on python-opencv >= 2.1
  * removed the buggy lines in detect.py (the module opencv in Ubuntu's
    package python-opencv 2.0 never provided the function cv.loadFile)
  * fixed some license issues, thanks to Luca Falavigna's reminders:
    - inserted the full text of CC-BY-SA 3.0, formated it in a 78-colum
      text and added a few linebreaks;
    - replaced stale references to GPL V2 in some files to the newer
      licence GPL V3.
    Closes: #593130

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 26 Sep 2010 19:21:16 +0200

pymecavideo (5.2-6) unstable; urgency=low

  * added a test to create ~/.local/share/data when it does not exist
    previously

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 26 Sep 2010 18:58:51 +0200

pymecavideo (5.2-5) unstable; urgency=low

  * fixed typos in help-fr.xml
  * included modifications coming from the uspstream branch 5.2 (release 227)

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 12 Sep 2010 15:36:53 +0200

pymecavideo (5.2-4) unstable; urgency=low

  * downgraded the dependency on python-opencv to allow the installtion on 
    current Ubuntu distributions.

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 12 Sep 2010 12:40:43 +0200

pymecavideo (5.2-3) unstable; urgency=low

  * Modified table.py to insert tabs in every line exported to the
    paste buffer.

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 12 Sep 2010 12:11:02 +0200

pymecavideo (5.2-2) unstable; urgency=low

  * added a dependency on python-opencv
  * modified detect.py to adapt to the distribution's pecularity; this should
    make the package usable with Ubuntu and Debian

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 11 Sep 2010 22:27:58 +0200

pymecavideo (5.2-1) unstable; urgency=low

  * upgraded to the newer upstream version
  * modified the patch for src/pymecavideo.py accordingly
  * rewritten debian/rules to use dh_stuff
  * added build-dependencies on python-all, libqt4-dev

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 11 Sep 2010 14:31:01 +0200

pymecavideo (5.0-5) unstable; urgency=low

  * First publication in Debian. Closes: #593130
  * upgraded Standards-Version to 3.9.1 and compat to 7

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 15 Aug 2010 19:09:12 +0200

pymecavideo (5.0-4) unstable; urgency=low

  * added a Recommends: line, to enforce the installation of a suitable
    video viewer, so you can enjoy the generated video file.
  * added some <refmiscinfo> lines to the manpage source, to comply with
    the specification of docbook's manual pages. Changed the copyright
    datespan.
  * written a French manpage and refreshed the default English manpage.

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 13 May 2010 16:24:22 +0200

pymecavideo (5.0-3) unstable; urgency=low

  * upgraded to the official version 5.0, from branches/5.0

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 03 May 2010 17:34:30 +0200

pymecavideo (5.0-2) unstable; urgency=low

  * fixed pymecavideo.desktop

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 15 Apr 2010 10:47:28 +0200

pymecavideo (5.0-1) unstable; urgency=low

  * upgraded to the last upstream version

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 14 Apr 2010 23:14:21 +0200

pymecavideo (4.1-1) unstable; urgency=low

  * upgraded to the last upstream version
  * installed the quilt patch system

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 21 Jan 2010 08:03:08 +0100

pymecavideo (4.0-5) unstable; urgency=low

  * replaced the invokation of evince by xdg-open, and adjusted the 
    dependencies accordingly
  * modified the calling scheme for the ps-viewer, since xdg-open
    returns before the ps-viewer quits.
  * added new rules for the autozoom behavior

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 18 Jan 2010 16:24:35 +0100

pymecavideo (4.0-4) unstable; urgency=low

  * made a few improvements for the restoration from a file ... 
    still unsufficient.
  * implemented plotting for the variations of x, y and velocity

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 16 Jan 2010 14:24:23 +0100

pymecavideo (4.0-3) unstable; urgency=low

  * fixed the error in the saved config file when a bad file has been 
    submitted.
  * added the analysis of non integer framerates.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 12 Jan 2010 17:53:21 +0100

pymecavideo (4.0-2) unstable; urgency=low

  * Fixed the bugs highlighted by Thibault North: now we use the built-in 
    class set, and the framerate is computed with a more secure method,
    relying on ffmpeg's messages.

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 12 Jan 2010 09:29:59 +0100

pymecavideo (4.0-1) unstable; urgency=low

  * Upgraded to the new version

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 11 Jan 2010 01:10:16 +0100

pymecavideo (3.2-3) unstable; urgency=low

  * enforced the correct use of pyshared

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 09 Nov 2009 00:51:10 +0100

pymecavideo (3.2-2) unstable; urgency=low

  * updated the packaging to comply with the new Debian-Python policy

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 08 Nov 2009 11:40:06 +0100

pymecavideo (3.2-1) unstable; urgency=low

  * upgraded to the new upstream revision

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 11 Apr 2009 17:51:01 +0200

pymecavideo (3.1-2) unstable; urgency=low

  * added a Catalan translation, thanks to jramire7@xtec.cat
  * modified the Docbook help file, to use a SYSTEM catalog.

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 18 Feb 2009 23:52:41 +0100

pymecavideo (3.1-1) unstable; urgency=low

  * upgraded to the new upstream version

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 12 Nov 2008 20:18:27 +0100

pymecavideo (3.0-1) unstable; urgency=low

  * upgraded to the new upstream version

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 11 Nov 2008 10:19:13 +0100

pymecavideo (2.5-1) unstable; urgency=low

  * rebuilt some parts on top of python-eduwidgets

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 24 Jul 2008 09:35:29 +0200

pymecavideo (2.4-2) unstable; urgency=low

  * fixed an error with the debugger behavior
  * fixed an error with the movie making feature. Now the first image is
    the image where the first points were clicked.
  * added a new feature: earlier clicked points now appear on the video 
    during the acquisition of the trajectories. The speed vectors also
    appear with a scale 1 px for 1 m/s.

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 17 Jul 2008 13:47:37 +0200

pymecavideo (2.4-1) unstable; urgency=low

  * Upgraded to svn revision 190
  * Added some new features : management of preferences, debug level
  * Added the possibility to use other video players
  * Fixed a bug with sys.path which prevented pickle from finding the 
    class vecteur.

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 19 Jan 2008 20:02:59 +0100

pymecavideo (2.3-1) unstable; urgency=low

  * Upgraded to svn revision 181
  * added a new feature : making a video grounded on a mobile point.
  * implemented the save and reopen features (option: -f file)
  * cleaned some code

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 17 Jan 2008 00:14:09 +0100

pymecavideo (2.2-1) unstable; urgency=low

  * Upgraded to svn revision 178
  * Activated the arrows happening near the mouse cursor
  * Re-activated the zoom window (from version 1.6)
  * Simplified the code for the zoom window
  * Modified the dependency : transcode --> ffmpeg, for the video decoder

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 14 Jan 2008 00:45:56 +0100

pymecavideo (2.1-1) unstable; urgency=low

  * Upgraded to svn revision 161
  * cleaned the code, simplified the interface for the trajectories,
  * activated the speed representation

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 12 Jan 2008 23:41:40 +0100

pymecavideo (2.0-1) unstable; urgency=low

  * Upgraded to revision 151 (trunk)

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 10 Jan 2008 22:29:12 +0100

pymecavideo (1.6-3) unstable; urgency=low

  * did the previous change effective (ooops).

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 10 Jan 2008 21:25:57 +0100

pymecavideo (1.6-2) unstable; urgency=low

  * modified a dependency: x-www-browser -> www-browser.

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 09 Jan 2008 18:52:46 +0100

pymecavideo (1.6-1) unstable; urgency=low

  * Upgraded to svn revision 104

 -- Georges Khaznadar <georgesk@ofset.org>  Sun, 06 Jan 2008 20:56:33 +0100

pymecavideo (1.5-1) unstable; urgency=low

  * Upgraded to svn revision 96
  * modified the code to make a link between label_point and 
    label_vitesse
  * upgraded to svn revision 98 which brought the mouse-hover events
  * enabled the show/hide feature for speed vectors in the neighborhood
    of the mouse pointer. (known bug : the speed vectors are not erased 
    consistently, for example changing the scale leaves the older 
    vectors)

 -- Georges Khaznadar <georgesk@ofset.org>  Sat, 05 Jan 2008 15:45:41 +0100

pymecavideo (1.4-1) unstable; urgency=low

  * Upgraded to svn revision 82
  * Cleaned the code to draw speed vectors
  * added a new feature to choose a scale for the speed vectors
  * cleaned the code for writing into the tables
  * cleaned the index of the dictionary dic_donnees : removed the string 
    'data-' which was prepended then trimmed and prepended again.

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 03 Jan 2008 16:04:54 +0100

pymecavideo (1.3-1) unstable; urgency=low

  * Upgraded to svn revision 73
  * Added some contents into the help files.

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 02 Jan 2008 01:21:57 +0100

pymecavideo (1.2-1) unstable; urgency=low

  * Upgraded to svn revision 69
  * mentioned the video/ dir in setup.py
  * made the current directory the default to read files
  * added a menu item Fichier->Exemples... to read example videos
  * added a desktop file
  * added a help file, with some screenshots

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 01 Jan 2008 23:10:31 +0100

pymecavideo (1.1-1) unstable; urgency=low

  * Upgraded to svn revision 66
  * Added a feedback for the scale (pixels by meter)
  * Allowed the scale to be redefined while the points are not grabbed
  * shortened the column labels in the table, adjusted their width
  * The measurements in the first panel are now in pixel unit, the
    second panel bears the same data in meter.
  * The saved files are named *.{txt|asc|dat|csv} for data in seconds
    and meters. A file named <samefile.{txt|asc|dat|csv}>.mecavideo is
    saved too, with data in seconds and pixels.
  * improved the save verification system, by using a "dirty" flag

 -- Georges Khaznadar <georgesk@ofset.org>  Tue, 01 Jan 2008 01:22:34 +0100

pymecavideo (1.0-1) unstable; urgency=low

  * Upgraded to svn version 54
  * fixed debian/rules
  * removed unnecessary python files
  * reorganized the main part to make a module pymecavideo, then invoke
    it from a script in /usr/bin
  * modified the export routine to mark the comments
  * cleaned the code for the decimal separator (taking in account the 
    locale)

 -- Georges Khaznadar <georgesk@ofset.org>  Mon, 31 Dec 2007 00:41:49 +0100

pymecavideo (0.3-1) unstable; urgency=low

  * Upgraded to snv revision 32

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 28 Dec 2007 22:52:29 +0100

pymecavideo (0.2-2) unstable; urgency=low

  * Added GPL-3 headings according to JB's intention.
  * Added the dependency on transcode

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 27 Dec 2007 15:15:36 +0100

pymecavideo (0.2-1) unstable; urgency=low

  * Upgraded to svn revision 23

 -- Georges Khaznadar <georgesk@ofset.org>  Thu, 27 Dec 2007 14:33:16 +0100

pymecavideo (0.1-1) unstable; urgency=low

  * Initial release 
  * Removed the script magic numbers from the modules
  * Ajouté la prise en compte d'un paramètre : le nom de fichier à ouvrir
  * Ajouté l'exportation dans un fichier texte.

 -- Georges Khaznadar <georgesk@ofset.org>  Wed, 26 Dec 2007 15:55:42 +0100

