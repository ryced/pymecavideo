<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="es" sourcelanguage="">
<context encoding="UTF-8">
    <name>Dialog</name>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Préférences de pyMecaVideo</source>
        <translation type="obsolete">Preferencias de pyMecaVideo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Échelle des vitesses (px pour 1m/s)</source>
        <translation type="obsolete">Escala de velocidades (px para 1m/s)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Vitesses affichées</source>
        <translation type="obsolete">Velocidades mostradas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Afficheur vidéo</source>
        <translation type="obsolete">Display video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Niveau de verbosité (débogage)</source>
        <translation type="obsolete">Nivel de debug</translation>
    </message>
</context>
<context>
    <name>Label_Echelle</name>
    <message>
        <location filename="" line="144"/>
        <source>Choisir le nombre de points puis &quot;D&#xc3;&#xa9;marrer l&apos;acquisition&quot; </source>
        <translation type="obsolete">Escojer el número de puntos y &quot;iniciar la acquisición&quot;</translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="379"/>
        <source>ind&#xc3;&#xa9;f.</source>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>chemin vers les modules : %s</source>
        <translation type="obsolete">Carpeta de los Módulos</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1041"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="obsolete">tiempo en segundos, posición en metros</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>point N&#xc2;&#xb0; </source>
        <translation type="obsolete">Point N° </translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Cliquer sur le point N&#xc2;&#xb0;%d</source>
        <translation type="obsolete">Clic en el punto N° :</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Definir la escala</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="obsolete">¿Cuál es el tamaño , en metros de su calibrador en la imagen?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1536"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="obsolete">Usted tiene que dar una escala válida</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="obsolete">Los datos serán perdidos</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Su trabajo no ha sido guardado.
¿Quiere Usted guardarlo ahora?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1614"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Usted alcanzó el principio del video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1650"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Abrir un video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Nombre de archivo no valido</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
 Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">El nombre de su archivo contiene caracteres con acentos o espacios. Por favor retirenlos antes de seguir.</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1677"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Usted debe escojer una imagen y definir la escala</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour ce langage %s.</source>
        <translation type="obsolete">Lo siento, no hay archivo de ayuda para este idioma</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Impossible de lire %s</source>
        <translation type="obsolete">Imposible de leer %s</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Usage : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</source>
        <translation type="obsolete">Uso : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>point N&#xc2;&#xb0;</source>
        <translation type="obsolete">punto N°</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="31"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation>PyMecaVideo, análisis mecánica de los videos</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="71"/>
        <source>Acquisition des données</source>
        <translation>Acquisición de datos</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="174"/>
        <source>Pas de vidéos chargées</source>
        <translation>No se ha cargado ningún video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Bienvenue sur pymeca vidéo, pas d&apos;images chargée</source>
        <translation type="obsolete">Bienvenidos en pymecavideo, no se ha cargado ningún video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="549"/>
        <source>Définir l&apos;échelle</source>
        <translation>Definir la escala</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="214"/>
        <source>Image n°</source>
        <translation>Imagen n°</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Nombre de points à étudier</source>
        <translation type="obsolete">Numero de puntos a estudiar</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.py" line="380"/>
        <source>indéf.</source>
        <translation>indef.</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="562"/>
        <source>px/m</source>
        <translation>px/m</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Démarrer l&apos;acquisition</source>
        <translation type="obsolete">Iniciar la acquisiciónf</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="533"/>
        <source>Tout réinitialiser</source>
        <translation>Reinicializar a todo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>efface le point précédent</source>
        <translation type="obsolete">elimina el punto precedente</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="453"/>
        <source>rétablit le point suivant</source>
        <translation>recupera el punto precedente</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>trajectoires et mesures</source>
        <translation type="obsolete">trayectorias y medidas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Origine du référentiel :</source>
        <translation type="obsolete">Origen del referencial</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Vidéo calculée</source>
        <translation type="obsolete">Video calculado</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>V. normale</source>
        <translation type="obsolete">V. normal</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>ralenti /2</source>
        <translation type="obsolete">velocidad 1/2</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>ralenti /4</source>
        <translation type="obsolete">velocidad 1/4</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>ralenti /8</source>
        <translation type="obsolete">velocidad 1/8</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Échelle de vitesses :</source>
        <translation type="obsolete">Escala de velocidades</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="830"/>
        <source>px pour 1 m/s</source>
        <translation>px para 1 m/s</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="926"/>
        <source>Coordonnées</source>
        <translation>Coordenadas</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="938"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation>Copiar medidas </translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Fichier</source>
        <translation type="obsolete">Archivo</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Aide</source>
        <translation type="obsolete">Ayuda</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Édition</source>
        <translation type="obsolete">Edicion</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.py" line="1652"/>
        <source>Ouvrir une vidéo</source>
        <translation type="unfinished">Abrir un video</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1064"/>
        <source>avanceimage</source>
        <translation>adelanta imagen</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1069"/>
        <source>reculeimage</source>
        <translation>retraza imagen</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>Quitter</source>
        <translation type="obsolete">Salir</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Enregistrer les données</source>
        <translation type="obsolete">Guardar datos</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>À propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1103"/>
        <source>Exemples ...</source>
        <translation type="unfinished">Ejemplos</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Rouvrir un fichier mecavidéo</source>
        <translation type="obsolete">Reabrir un archivo mecavideo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="" line="144"/>
        <source>Préférences</source>
        <translation type="obsolete">Preferencias</translation>
    </message>
    <message>
        <location filename="" line="144"/>
        <source>copier dans le presse-papier</source>
        <translation type="obsolete">copiar</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="265"/>
        <source>Acquisition video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="300"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="367"/>
        <source>Acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="382"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="416"/>
        <source>efface la série précédente</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="619"/>
        <source>Points à 
 étudier:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="637"/>
        <source>suivi
automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="648"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="658"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="666"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="676"/>
        <source>Trajectoires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="780"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="789"/>
        <source>près de
la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="800"/>
        <source>partout</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="807"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="860"/>
        <source>Voir un graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1264"/>
        <source>Choisir ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="891"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="920"/>
        <source>Définir un autre référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="932"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="946"/>
        <source>Exporter vers ....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="951"/>
        <source>Oo.o Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="956"/>
        <source>Qtiplot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="961"/>
        <source>SciDAVis</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="969"/>
        <source>changer d&apos;échelle ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1009"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1013"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1030"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1039"/>
        <source>&amp;Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1056"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1074"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1082"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1090"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1095"/>
        <source>Aide (F1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1108"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1113"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1118"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1126"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1134"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1142"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1147"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1152"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="278"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="309"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="353"/>
        <source>NON DISPO : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1042"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="unfinished">tiempo en segundos, posición en metros</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1105"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1116"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1347"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1378"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="unfinished">Definir la escala</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="unfinished">¿Cuál es el tamaño , en metros de su calibrador en la imagen?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1538"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="unfinished">Usted tiene que dar una escala válida</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="unfinished">Los datos serán perdidos</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="unfinished">Su trabajo no ha sido guardado.
¿Quiere Usted guardarlo ahora?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1616"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished">Usted alcanzó el principio del video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Nom de fichier non conforme</source>
        <translation type="unfinished">Nombre de archivo no valido</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1679"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="unfinished">Usted debe escojer una imagen y definir la escala</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1713"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="cadreur.py" line="56"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="echelle.py" line="146"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="echelle.py" line="151"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
