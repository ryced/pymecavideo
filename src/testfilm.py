#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import threading
import os.path

if sys.platform == "win32":
    import cv2 as cv
else:
    import cv


class film:
    """
    Une classe pour accéder aux images d'un film
    """

    def __init__(self, filename):
        """
        le constructeur
        @param filename le nom d'un fichier video
        """
        try:
            filename = unicode(filename, 'utf8')
        except TypeError:
            pass
        self.filename = filename
        
        
        try:
            self.filesize = os.path.getsize(self.filename.encode('utf8'))
            if sys.platform == "win32":
                self.capture = cv.VideoCapture(self.filename.encode('utf8'))
            else:
                self.capture = cv.CreateFileCapture(self.filename.encode('utf8'))
        except WindowsError:
            self.filesize = os.path.getsize(self.filename.encode('cp1252'))
            if sys.platform == "win32":
                self.capture = cv.VideoCapture(self.filename.encode('cp1252'))
            else:
                self.capture = cv.CreateFileCapture(self.filename.encode('cp1252'))

        t = threading.Thread(target=self.autoTest)
        t.start()
        t.join(5.0)  # attente de 5 secondes au plus


    def autoTest(self):
        self.ok = False

        try:
            if sys.platform == "win32":
                self.frame = self.capture.read()[1]
            else:
                self.frame = cv.QueryFrame(self.capture)
            self.num = 0
            if sys.platform == "win32":
                self.fps = self.capture.get(5)
                self.framecount = self.capture.get(7)
            else:
                self.fps = cv.GetCaptureProperty(self.capture, cv.CV_CAP_PROP_FPS)
                self.framecount = cv.GetCaptureProperty(self.capture, cv.CV_CAP_PROP_FRAME_COUNT)
    #        print "fps :",self.fps
    #        print "framecount :", self.framecount
            assert 1.0 * self.filesize / self.framecount > 1800.0, "fichier aberrant en taille"
            self.ok = True
        except AssertionError:

            pass
        except ZeroDivisionError:
            pass
        if self.filename.split('.')[-1].lower() == "ogv":  # never work with ogv. need encoding.
            self.ok = False

    def __int__(self):
        return int(self.ok)

    def __nonzero__(self):
        return self.ok


if __name__ == '__main__':
    if len(sys.argv) > 1:
        vidfile = sys.argv[1]
        if film(vidfile):
            sys.exit(0)
        else:
            sys.exit(1)

