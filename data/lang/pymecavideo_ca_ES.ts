<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>pymecavideo</name>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="31"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="71"/>
        <source>Acquisition des données</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="174"/>
        <source>Pas de vidéos chargées</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="214"/>
        <source>Image n°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="265"/>
        <source>Acquisition video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="300"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="367"/>
        <source>Acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="382"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="416"/>
        <source>efface la série précédente</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="453"/>
        <source>rétablit le point suivant</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="533"/>
        <source>Tout réinitialiser</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="549"/>
        <source>Définir l&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="562"/>
        <source>px/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.py" line="380"/>
        <source>indéf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="619"/>
        <source>Points à 
 étudier:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="637"/>
        <source>suivi
automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="648"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="658"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="666"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="676"/>
        <source>Trajectoires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="780"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="789"/>
        <source>près de
la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="800"/>
        <source>partout</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="807"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="830"/>
        <source>px pour 1 m/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="860"/>
        <source>Voir un graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1264"/>
        <source>Choisir ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="891"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="920"/>
        <source>Définir un autre référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="926"/>
        <source>Coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="932"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="938"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="946"/>
        <source>Exporter vers ....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="951"/>
        <source>Oo.o Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="956"/>
        <source>Qtiplot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="961"/>
        <source>SciDAVis</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="969"/>
        <source>changer d&apos;échelle ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1009"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1013"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1030"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1039"/>
        <source>&amp;Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1056"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1064"/>
        <source>avanceimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1069"/>
        <source>reculeimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1074"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1082"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1090"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1095"/>
        <source>Aide (F1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1103"/>
        <source>Exemples ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1108"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1113"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1118"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1126"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1134"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1142"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1147"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1152"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="278"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="309"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="353"/>
        <source>NON DISPO : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1042"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1105"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1116"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1347"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1378"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1538"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1616"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1652"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Nom de fichier non conforme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1679"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1713"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="cadreur.py" line="56"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="echelle.py" line="146"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="echelle.py" line="151"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
