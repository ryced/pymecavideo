<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en" sourcelanguage="fr">
<context>
    <name>@default</name>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>Dialog</name>
    <message encoding="UTF-8">
        <location filename="preferences.ui" line="13"/>
        <source>Préférences de pyMecaVideo</source>
        <translation type="obsolete">Preferences for Pymecavideo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="preferences.ui" line="19"/>
        <source>Échelle des vitesses (px pour 1m/s)</source>
        <translation type="obsolete">Scale for velocities (px by m/s)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="preferences.ui" line="29"/>
        <source>Vitesses affichées</source>
        <translation type="obsolete">Display velocities</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="preferences.ui" line="39"/>
        <source>Afficheur vidéo</source>
        <translation type="obsolete">Video player</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="preferences.ui" line="49"/>
        <source>Niveau de verbosité (débogage)</source>
        <translation type="obsolete">Verbosity level (debugging)</translation>
    </message>
</context>
<context>
    <name>Label_Echelle</name>
    <message>
        <location filename="echelle.py" line="109"/>
        <source>Choisir le nombre de points puis &quot;D&#xc3;&#xa9;marrer l&apos;acquisition&quot; </source>
        <translation type="obsolete">Choose the number of points then \&quot;Start aquisition\&quot;</translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="379"/>
        <source>ind&#xc3;&#xa9;f.</source>
        <translation type="obsolete">undef.</translation>
    </message>
    <message>
        <location filename="__init__.py" line="336"/>
        <source>chemin vers les modules : %s</source>
        <translation type="obsolete">path to the modules: %s</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1041"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="obsolete">time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="__init__.py" line="448"/>
        <source>point N&#xc2;&#xb0; </source>
        <translation type="obsolete">Point N° </translation>
    </message>
    <message>
        <location filename="__init__.py" line="722"/>
        <source>Cliquer sur le point N&#xc2;&#xb0;%d</source>
        <translation type="obsolete">Clic on the point #%d</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Define a scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="obsolete">Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1536"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="obsolete">Please give a valid scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="obsolete">Data will be lost</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Your work has not been saved. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1614"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1650"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Open a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Unvalid filename</translation>
    </message>
    <message>
        <location filename="__init__.py" line="895"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
 Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Your filename contains accented characters or spaces. Please rename it before going further</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1677"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="__init__.py" line="938"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour ce langage %s.</source>
        <translation type="obsolete">Sorry, no help file for this language %s </translation>
    </message>
    <message>
        <location filename="__init__.py" line="1010"/>
        <source>Impossible de lire %s</source>
        <translation type="obsolete">Read %s failed</translation>
    </message>
    <message>
        <location filename="__init__.py" line="1029"/>
        <source>Usage : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</source>
        <translation type="obsolete">Usage : pymecavideo [-f file | --fichier_pymecavideo=file]</translation>
    </message>
    <message>
        <location filename="__init__.py" line="438"/>
        <source>point N&#xc2;&#xb0;</source>
        <translation type="obsolete">point #</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="308"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="obsolete">undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="352"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="obsolete">Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1104"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">point # %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1115"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1262"/>
        <source>Choisir ...</source>
        <translation type="obsolete">Choose...</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation type="obsolete">Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1345"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1376"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">You reached the end of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1627"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1636"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">video files( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov) </translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1711"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="obsolete">Sorry, no help file for the language %1.</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="31"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation>Pymecavideo, mechanical analysis of video clips</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="71"/>
        <source>Acquisition des données</source>
        <translation>Data acquisition</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="174"/>
        <source>Pas de vidéos chargées</source>
        <translation>No video loaded</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="144"/>
        <source>Bienvenue sur pymeca vidéo, pas d&apos;images chargée</source>
        <translation type="obsolete">Welcome in pymecavideo, no video loaded</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="549"/>
        <source>Définir l&apos;échelle</source>
        <translation>Define the scale</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="214"/>
        <source>Image n°</source>
        <translation>Image #</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="246"/>
        <source>Nombre de points à étudier</source>
        <translation type="obsolete">Number of points to study</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.py" line="380"/>
        <source>indéf.</source>
        <translation>undef.</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="562"/>
        <source>px/m</source>
        <translation>px/m</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="297"/>
        <source>Démarrer l&apos;acquisition</source>
        <translation type="obsolete">Start acquisition</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="533"/>
        <source>Tout réinitialiser</source>
        <translation>Reinit everything</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="390"/>
        <source>efface le point précédent</source>
        <translation type="obsolete">delete previous points</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="453"/>
        <source>rétablit le point suivant</source>
        <translation>restore next points</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="426"/>
        <source>trajectoires et mesures</source>
        <translation type="obsolete">trajectory and measurements</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="526"/>
        <source>Origine du référentiel :</source>
        <translation type="obsolete">Origin of the axis:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="560"/>
        <source>Vidéo calculée</source>
        <translation type="obsolete">Computed video</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="568"/>
        <source>V. normale</source>
        <translation type="obsolete">Normal Vel</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="573"/>
        <source>ralenti /2</source>
        <translation type="obsolete">slower /2</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="578"/>
        <source>ralenti /4</source>
        <translation type="obsolete">slower /4</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="583"/>
        <source>ralenti /8</source>
        <translation type="obsolete">slower /8</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="608"/>
        <source>Échelle de vitesses :</source>
        <translation type="obsolete">Scale for velocities:</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="830"/>
        <source>px pour 1 m/s</source>
        <translation>px for 1 m/s</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="926"/>
        <source>Coordonnées</source>
        <translation>Coordinates</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="938"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation>Copy data to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="682"/>
        <source>Fichier</source>
        <translation type="obsolete">File</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="753"/>
        <source>Aide</source>
        <translation type="obsolete">Help</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="703"/>
        <source>Édition</source>
        <translation type="obsolete">Edit</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.py" line="1652"/>
        <source>Ouvrir une vidéo</source>
        <translation>Open a video</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1064"/>
        <source>avanceimage</source>
        <translation>imgforward</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1069"/>
        <source>reculeimage</source>
        <translation>imgbackward</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="738"/>
        <source>Quitter</source>
        <translation type="obsolete">Quit</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="743"/>
        <source>Enregistrer les données</source>
        <translation type="obsolete">Save data</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="748"/>
        <source>À propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1103"/>
        <source>Exemples ...</source>
        <translation>Examples...</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="763"/>
        <source>Rouvrir un fichier mecavidéo</source>
        <translation type="obsolete">Reopen a mecavideo file</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo.ui" line="768"/>
        <source>Préférences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="773"/>
        <source>copier dans le presse-papier</source>
        <translation type="obsolete">copy to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="265"/>
        <source>Acquisition video</source>
        <translation>Acquire vIdeo</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="300"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="367"/>
        <source>Acquisition</source>
        <translation>Acquire</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="382"/>
        <source>Démarrer</source>
        <translation>Start</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="416"/>
        <source>efface la série précédente</source>
        <translation>delete previous series</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="619"/>
        <source>Points à 
 étudier:</source>
        <translation>Points to study:</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="637"/>
        <source>suivi
automatique</source>
        <translation>automatic tracking</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="648"/>
        <source>Changer d&apos;origine</source>
        <translation>Change the origin</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="658"/>
        <source>Abscisses 
vers la gauche</source>
        <translation>Abscissa to the left</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="666"/>
        <source>Ordonnées 
vers le bas</source>
        <translation>Ordinate to the bottom</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="676"/>
        <source>Trajectoires</source>
        <translation>Trajectories</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="780"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation>Show velocity vectors</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="789"/>
        <source>près de
la souris</source>
        <translation>near the mouse</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="800"/>
        <source>partout</source>
        <translation>everywhere</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="807"/>
        <source>Échelle de vitesses</source>
        <translation>Scale for velocity</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="860"/>
        <source>Voir un graphique</source>
        <translation>View a plot</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1264"/>
        <source>Choisir ...</source>
        <translation>Choose...</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="891"/>
        <source>Voir la vidéo</source>
        <translation>View video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="920"/>
        <source>Définir un autre référentiel : </source>
        <translation>Define another reference: </translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="932"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation>Table of dates and coordinates</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="946"/>
        <source>Exporter vers ....</source>
        <translation>Export to...</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="951"/>
        <source>Oo.o Calc</source>
        <translation>Oo.o Calc</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="956"/>
        <source>Qtiplot</source>
        <translation>Qtiplot</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="961"/>
        <source>SciDAVis</source>
        <translation>SciDAVis</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="969"/>
        <source>changer d&apos;échelle ?</source>
        <translation>change scale?</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1009"/>
        <source>&amp;Fichier</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1013"/>
        <source>E&amp;xporter vers ...</source>
        <translation>E&amp;xport to...</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1030"/>
        <source>&amp;Aide</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1039"/>
        <source>&amp;Edition</source>
        <translation>&amp;Edit</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1056"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation>&amp;Open video (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1074"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation>Quit (Ctrl-Q)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1082"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation>Save data (Ctrl-S)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1090"/>
        <source>À &amp;propos</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1095"/>
        <source>Aide (F1)</source>
        <translation>Help (F1)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1108"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation>Open a &amp;mecavideo project</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1113"/>
        <source>&amp;Préférences</source>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1118"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation>&amp;Copy to the clipboard (Ctrl-C)</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pymecavideo_mini.ui" line="1126"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation>Undo (Ctrl-Z)</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1134"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation>Redo (Ctrl-Y)</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1142"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation>OpenOffice &amp;Calc</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1147"/>
        <source>Qti&amp;plot</source>
        <translation>Qti&amp;plot</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1152"/>
        <source>Sci&amp;davis</source>
        <translation>Sci&amp;davis</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="278"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation>Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="309"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation>undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="353"/>
        <source>NON DISPO : %1</source>
        <translation>NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation>Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="870"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation>pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1042"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation>time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1105"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation>point # %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1116"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation>PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation>Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation>Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1285"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation>Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1347"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation>Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1378"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation>You reached the end of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation>Define a scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1520"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation>Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1538"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation>Please give a valid scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation>Data will be lost</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1594"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation>Your work has not been saved. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1616"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation>You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Nom de fichier non conforme</source>
        <translation>Unvalid filename</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1647"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation>The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1679"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation>Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1713"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation>Sorry, no help file for the language %1.</translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="cadreur.py" line="56"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation>Type Esc to quit</translation>
    </message>
    <message>
        <location filename="echelle.py" line="146"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation>Choose the number of points then &quot;start acquisition&quot;</translation>
    </message>
    <message>
        <location filename="echelle.py" line="151"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation>You can keep on acquiring</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation>Near the mouse %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation>: last video %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation>: videoDir %1</translation>
    </message>
</context>
</TS>
